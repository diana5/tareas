package com.tares.tareas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tares.tareas.model.UsuarioRol;

public interface UsuarioRolRepository extends JpaRepository<UsuarioRol,Integer> {

}
