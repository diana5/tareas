package com.tares.tareas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tares.tareas.model.Empleado;

public interface EmpleadoRepository extends JpaRepository<Empleado,Integer>{}