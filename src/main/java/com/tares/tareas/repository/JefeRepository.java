package com.tares.tareas.repository;

import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tares.tareas.model.Jefe;
@Transactional
public interface JefeRepository extends JpaRepository<Jefe,Integer> {



}
