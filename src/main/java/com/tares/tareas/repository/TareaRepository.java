package com.tares.tareas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tares.tareas.model.Tarea;

public interface TareaRepository extends JpaRepository<Tarea,Integer>{

}
