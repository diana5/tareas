package com.tares.tareas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tares.tareas.model.CategoriaTarea;

public interface CategoriaTareaRepository extends JpaRepository<CategoriaTarea, Integer>{

}
