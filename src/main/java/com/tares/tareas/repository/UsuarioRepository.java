package com.tares.tareas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.tares.tareas.model.Usuario;

public interface UsuarioRepository  extends JpaRepository<Usuario,Integer>{


	Usuario findByUsername(String userId);


}
