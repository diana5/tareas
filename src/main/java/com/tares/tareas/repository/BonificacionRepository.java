package com.tares.tareas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tares.tareas.model.Bonificacion;

public interface BonificacionRepository extends JpaRepository<Bonificacion,Integer> {

}
