package com.tares.tareas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name="Bonificacion")
public class Bonificacion {


	@GeneratedValue
	@Id
	@Column(name="id_bonificacion")
	public Integer id_bonificacion;
	
	@Column (name="bonificacion")
	public Integer bonificacion;
	
// una tarea tiene una bonificacion y es mapeada por un objeto tarea
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tarea")
    private Tarea tarea;

	public Integer getId_bonificacion() {
		return id_bonificacion;
	}

	public Integer getBonificacion() {
		return bonificacion;
	}

	public void setId_bonificacion(Integer id_bonificacion) {
		this.id_bonificacion = id_bonificacion;
	}

	public void setBonificacion(Integer bonificacion) {
		this.bonificacion = bonificacion;
	}
	
	public Tarea getTarea() {
		return tarea;
	}

	public void setTarea(Tarea tarea) {
		this.tarea = tarea;
	}
}
