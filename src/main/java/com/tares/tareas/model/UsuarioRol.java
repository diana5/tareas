package com.tares.tareas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="UsuarioRol")
public class UsuarioRol {
	@Column(name="id_rol")
	@Id
	Integer id_rol;
	
	@Column(name="descripcion")
	String descripcion;

	public Integer getId_rol() {
		return id_rol;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setId_rol(Integer id_rol) {
		this.id_rol = id_rol;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	
}
