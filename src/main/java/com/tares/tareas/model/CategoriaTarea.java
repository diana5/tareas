package com.tares.tareas.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity(name="categoriaTarea")
public class CategoriaTarea {
	@Column(name="cat_tarea_id")
	@Id
	Integer cat_tarea_id; 
	
	@Column(name="descripcion")
	String descripcion;
	
	@OneToOne(cascade = CascadeType.ALL)
	public EstadoTarea tarea;

	public Integer getId() {
		return cat_tarea_id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setId(Integer id) {
		this.cat_tarea_id = id;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
