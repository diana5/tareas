package com.tares.tareas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name="img_jefes")
public class ImgJefes {
	@Column(name=" img_id")
	@Id
	Integer img_id;
	
	@Column(name="nombre")
	String nombre;

	  @OneToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "id_jefe")
	    private Jefe jefe;
	
	public Integer getImg_id() {
		return img_id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setImg_id(Integer img_id) {
		this.img_id = img_id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


}
