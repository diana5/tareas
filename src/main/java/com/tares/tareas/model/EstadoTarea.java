package com.tares.tareas.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

@Entity(name="EstadoTarea")
public class EstadoTarea {

@Column(name="estado_id")
@Id
Integer estado_id;

@Column(name="descripcion")
String descripcion;




public Integer getEstado_id() {
	return estado_id;
}

public String getDescripcion() {
	return descripcion;
}

public void setEstado_id(Integer estado_id) {
	this.estado_id = estado_id;
}

public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}




}
