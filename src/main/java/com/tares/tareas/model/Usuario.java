package com.tares.tareas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
@Inheritance
@Entity(name="usuario")
//la clase abstracta no se mapea en la bd sino que mapea todas sus tablas en las clases hijas
public class Usuario {

	@Column(name="usuario_id")
	@Id
	@GeneratedValue
	Integer usuario_id;
	
	@Column (name="departamento")
	String departamento;
	
	@Column (name="nombre")
	String nombre;
	
	@Column (name="apellido")
	String apellido;
	
	@Column (name="username")
	String username;
	
	@Column (name="password")
	String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getUsuario_id() {
		return usuario_id;
	}

	public String getDepartamento() {
		return departamento;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getUsername() {
		return username;
	}

	public void setUsuario_id(Integer usuario_id) {
		this.usuario_id = usuario_id;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	

}
