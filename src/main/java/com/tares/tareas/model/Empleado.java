package com.tares.tareas.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name="Empleado")
public class Empleado extends Usuario {


@Column(name="cargo")
public String cargo;

@Column(name="sueldo")
public Integer Sueldo;


//un empleado puede tener muchas tareas
@OneToMany(
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    private List<Tarea> comments = new ArrayList<>();





public Integer getSueldo() {
	return Sueldo;
}


public void setSueldo(Integer sueldo) {
	Sueldo = sueldo;
}


public String getCargo() {
	return cargo;
}


public void setCargo(String cargo) {
	this.cargo = cargo;
}
}
