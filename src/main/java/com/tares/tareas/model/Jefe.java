package com.tares.tareas.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity(name="jefe")
public class Jefe extends Usuario {
	
@Column (name="jefatura")
String jefatura;


public String getJefatura() {
	return jefatura;
}

public void setJefatura(String jefatura) {
	this.jefatura = jefatura;
}

//un jefe muchas tareas

@OneToMany(
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    private List<Tarea> comments = new ArrayList<>();


}
