package com.tares.tareas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name="ImgEmpleados")
public class ImgEmpleados {
@Column(name="imge_id")
@Id
Integer imge_id;

@Column(name="nombre")
String nombre;

@OneToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "empleado_id")
private Empleado empleado;



public Integer getImge_id() {
	return imge_id;
}

public String getNombre() {
	return nombre;
}

public void setImge_id(Integer imge_id) {
	this.imge_id = imge_id;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

}