package com.tares.tareas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name="calificacion")
public class Calificacion {

@Column(name="id_calificacion")
@GeneratedValue
@Id
Integer id_calificacion;

@Column(name="puntuacion")
Integer puntuacion;

@Column(name="comentario")
String comentario;

//una tareaa tiene una c alificacion y es mapeada por un objeto tarea
@OneToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "id_tarea")
private Tarea tarea;



public Integer getId_calificacion() {
	return id_calificacion;
}

public Tarea getTarea() {
	return tarea;
}

public void setId_calificacion(Integer id_calificacion) {
	this.id_calificacion = id_calificacion;
}

public void setTarea(Tarea tarea) {
	this.tarea = tarea;
}

public Integer getPuntuacion() {
	return puntuacion;
}

public String getComentario() {
	return comentario;
}



public void setPuntuacion(Integer puntuacion) {
	this.puntuacion = puntuacion;
}

public void setComentario(String comentario) {
	this.comentario = comentario;
}



}
