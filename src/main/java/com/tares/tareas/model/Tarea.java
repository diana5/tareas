package com.tares.tareas.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

@Entity(name="tarea")
public class Tarea {
@Column(name="id_tarea")
@Id
@GeneratedValue
Integer id_tarea;

@Column(name="tarea")
String tarea;

//muchas tareas para un jefe
@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "usuario_id" , insertable=false, updatable=false)
private Jefe jefe;


//muchas tareas para un empleado, el insertable=false, updatable=false es necesario en ambas
@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "usuario_id" ,insertable=false, updatable=false)
private Empleado empleado;


//Relacion Tarea tiene calificacion: ess mapeado por el obj tarea en la clase calificacion 
@OneToOne(mappedBy = "tarea", cascade = CascadeType.ALL,
fetch = FetchType.LAZY, optional = false)
private Calificacion calificacion;


//una tareaa tiene una Bonificacion y es mapeada por un objeto tarea en la clase bonificacion
@OneToOne(mappedBy = "tarea", cascade = CascadeType.ALL,
fetch = FetchType.LAZY, optional = false)
private Bonificacion bonificacion;


public Jefe getJefe() {
	return jefe;
}


public Empleado getEmpleado() {
	return empleado;
}


public Calificacion getCalificacion() {
	return calificacion;
}


public Bonificacion getBonificacion() {
	return bonificacion;
}


public void setJefe(Jefe jefe) {
	this.jefe = jefe;
}


public void setEmpleado(Empleado empleado) {
	this.empleado = empleado;
}


public void setCalificacion(Calificacion calificacion) {
	this.calificacion = calificacion;
}


public void setBonificacion(Bonificacion bonificacion) {
	this.bonificacion = bonificacion;
}


public Integer getId_tarea() {
	return id_tarea;
}


public String getTarea() {
	return tarea;
}


public void setId_tarea(Integer id_tarea) {
	this.id_tarea = id_tarea;
}


public void setTarea(String tarea) {
	this.tarea = tarea;
}


}
