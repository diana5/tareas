package com.tares.tareas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tares.tareas.model.UsuarioRol;
import com.tares.tareas.repository.UsuarioRolRepository;

@RestController
@RequestMapping("V1/usuariorol")
public class UsuarioRolController {
	@Autowired
	UsuarioRolRepository usuariorolRepository;
	
	//Obtener los Roles De Usuario
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<Object> obtenerRoles(@RequestParam ("page") int page, @RequestParam  ("size") int size){
		Page <UsuarioRol> rolesLista = usuariorolRepository.findAll(PageRequest.of(page, size));
		return new ResponseEntity<Object>(rolesLista, HttpStatus.OK);
	}

}
