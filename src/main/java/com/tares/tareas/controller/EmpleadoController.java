package com.tares.tareas.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tares.tareas.model.CategoriaTarea;
import com.tares.tareas.model.Empleado;
import com.tares.tareas.repository.EmpleadoRepository;

@RestController
@RequestMapping(value="V1/empleado")
public class EmpleadoController {
	
@Autowired
EmpleadoRepository empleadorepository;	

@RequestMapping(method=RequestMethod.GET)
public ResponseEntity<Object>listarEmpleados(@RequestParam("page") int page, @RequestParam("size") int size){
	Page<Empleado> empleadolista= empleadorepository.findAll(PageRequest.of(page, size));
	return new ResponseEntity<Object>(empleadolista,HttpStatus.OK);

}

@RequestMapping(method=RequestMethod.POST)
public ResponseEntity<Object> agregarEmpleados(@RequestBody @Valid Empleado empleado){

	
	if(empleado==null) {
		return new  ResponseEntity<Object>(empleado,HttpStatus.NO_CONTENT);
	}
	
	empleadorepository.save(empleado);
	
	return new ResponseEntity<Object> (empleado,HttpStatus.OK); }

@RequestMapping(method=RequestMethod.PUT,path="/{id}")
public ResponseEntity<Object> modificarEmpleados(@PathVariable("id") Integer id,
		@Valid @RequestBody Empleado  detalleEmpleado){
	Optional <Empleado> empleado= empleadorepository.findById(id);
	if(!empleado.isPresent()) 
		return new ResponseEntity<Object>( HttpStatus.NO_CONTENT);	
	
	//metodos delaclase empleado
	empleado.get().setCargo(detalleEmpleado.getCargo());
	empleado.get().setSueldo(detalleEmpleado.getSueldo());
	
	//metodos delaclase usuario
	empleado.get().setApellido(detalleEmpleado.getApellido());
	empleado.get().setNombre(detalleEmpleado.getNombre());
	empleado.get().setPassword(detalleEmpleado.getPassword());
	empleado.get().setUsername(detalleEmpleado.getUsername());
	empleadorepository.save(empleado.get());
	return new ResponseEntity<Object>(empleado.get(),HttpStatus.OK);
}

@RequestMapping(method=RequestMethod.DELETE,path="{/usuario_id}")
public ResponseEntity<Object> eliminarEmpleados(@PathVariable("usuario_id") Integer id){
	empleadorepository.deleteById(id);
	return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	
}




}

