package com.tares.tareas.controller;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tares.tareas.model.Empleado;
import com.tares.tareas.model.Jefe;
import com.tares.tareas.repository.EmpleadoRepository;
import com.tares.tareas.repository.JefeRepository;

	@RestController
	@RequestMapping(value="V1/jefe")
	public class JefeController {
		
	@Autowired
	JefeRepository jeferepository;
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<Object>listarJefes(@RequestParam("page") int page, @RequestParam("size") int size){
		Page<Jefe> jefelista= jeferepository.findAll(PageRequest.of(page, size));
	return new ResponseEntity<Object>(jefelista,HttpStatus.OK); }


	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Object> agregarEmpleados(@RequestBody @Valid Jefe jefe){
		jeferepository.save(jefe);
		
		if(jefe==null) {
			return new  ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<Object> (jefe, HttpStatus.OK); }

	@RequestMapping(method=RequestMethod.PUT,path="/{id}")
	public ResponseEntity<Object> modificarEmpleados(@PathVariable("id") Integer id,
			@Valid @RequestBody Jefe detalleJefe){
		Optional <Jefe> jefe= jeferepository.findById(id);
		if(!jefe.isPresent()) 
			return new ResponseEntity<Object>( HttpStatus.NO_CONTENT);	
		
		jefe.get().setDepartamento(detalleJefe.getDepartamento());
		jefe.get().setApellido(detalleJefe.getApellido());
		jefe.get().setJefatura(detalleJefe.getJefatura());
		jefe.get().setNombre(detalleJefe.getNombre());
		jefe.get().setPassword(detalleJefe.getPassword());
		jefe.get().setUsername(detalleJefe.getUsername());
		jefe.get().setUsuario_id(detalleJefe.getUsuario_id());
		jefe.get().setUsername(detalleJefe.getUsername());
		
		jeferepository.save(jefe.get());
		return new ResponseEntity<Object>(jefe.get(),HttpStatus.NO_CONTENT);
	}

	@RequestMapping(method=RequestMethod.DELETE,path="{/id}")
	public ResponseEntity<Object> eliminarEmpleados(@PathVariable("id") Integer id){
		jeferepository.deleteById(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		
	}
	

}