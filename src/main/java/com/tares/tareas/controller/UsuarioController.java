package com.tares.tareas.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tares.tareas.model.Usuario;
import com.tares.tareas.repository.UsuarioRepository;

@RestController
@RequestMapping(value="V1/usuario")
public class UsuarioController {
@Autowired
UsuarioRepository usuarioRepository;
@RequestMapping(method=RequestMethod.GET)
public ResponseEntity<Object> listarUsuarios(@RequestParam("size")int size, @RequestParam("page") int page){
	return new ResponseEntity<Object> (usuarioRepository.findAll(),HttpStatus.OK);
}



}
