package com.tares.tareas.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tares.tareas.model.Bonificacion;
import com.tares.tareas.repository.BonificacionRepository;

@RestController
@RequestMapping(value="V1/bonificacion")
public class BonificacionController {
	@Autowired 
	BonificacionRepository bonificacionrepository;
	
@RequestMapping(method=RequestMethod.GET)
public ResponseEntity<Object> listarBonificaciones(){
return new ResponseEntity<Object>(bonificacionrepository.findAll(),HttpStatus.OK); }

	
@RequestMapping(method=RequestMethod.POST)
public ResponseEntity<Object> agregarBonificaciones(@RequestBody @Valid Bonificacion bonificacion){
	bonificacionrepository.save(bonificacion);
	return new ResponseEntity<Object>(bonificacion,HttpStatus.OK);
}

@RequestMapping(method=RequestMethod.PUT)
public ResponseEntity<Object> modificarBonificacion(@PathVariable(value="id")Integer id,@Valid @RequestBody Bonificacion bonificacion){
	
	Optional <Bonificacion>bonificacionnueva= bonificacionrepository.findById(id);
	if(bonificacion==null)
	return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	bonificacionnueva.get().setBonificacion(bonificacion.getBonificacion()); 
	bonificacionnueva.get().setId_bonificacion(bonificacion.getBonificacion());
	bonificacionnueva.get().setTarea(bonificacion.getTarea());
	
	bonificacionrepository.save(bonificacionnueva.get());
	
	return new ResponseEntity<Object>(bonificacion,HttpStatus.OK);
}

	@RequestMapping(method=RequestMethod.DELETE,path="{/id}")
	public ResponseEntity<Object> eliminarBonificaciones(@PathVariable(value="id") Integer id){
	bonificacionrepository.deleteById(id);
	return new  ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}
	
	

}
