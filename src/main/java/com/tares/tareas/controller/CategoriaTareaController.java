package com.tares.tareas.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tares.tareas.model.CategoriaTarea;
import com.tares.tareas.repository.CategoriaTareaRepository;


@RestController
@RequestMapping(value="V1/Categoria")
public class CategoriaTareaController {
	
	@Autowired
	CategoriaTareaRepository categoriatareaRepository;

	//get all categories
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<Object> listar(@RequestParam("page") int page, @RequestParam("size") int size){
		
		Page<CategoriaTarea> lista=categoriatareaRepository.findAll(PageRequest.of(page, size));
		
		return new ResponseEntity<Object>(lista,HttpStatus.OK);
	}
	
	//post category
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Object> agregarCategoria(@RequestBody @Valid CategoriaTarea categoria){
		categoriatareaRepository.save(categoria);
		
		if (categoria==null)
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	//put
	
	@RequestMapping(method=RequestMethod.PUT,path="{/id}")
	public ResponseEntity<Object> modificarCategoria(@PathVariable(value="id") Integer idcategoria,
			@Valid @RequestBody CategoriaTarea detallecategoria){
		Optional <CategoriaTarea> categoria= categoriatareaRepository.findById(idcategoria);
		if(!categoria.isPresent()) 
			return new ResponseEntity<Object>( HttpStatus.NO_CONTENT);
		
		categoria.get().setDescripcion(detallecategoria.getDescripcion());
		categoria.get().setId(detallecategoria.getId());
		
		
		return new ResponseEntity<Object>(categoria.get(),HttpStatus.OK);
		
	}
	
	//delete 
	
	@RequestMapping(method=RequestMethod.DELETE,path="{/id}")
	public  ResponseEntity<Object> eliminar(@PathVariable(value="id")Integer id){
	 categoriatareaRepository.deleteById(id);
	 return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}
	

}
