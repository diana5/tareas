package com.tares.tareas.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tares.tareas.model.Tarea;
import com.tares.tareas.repository.TareaRepository;

@RestController
@RequestMapping(value="V1/tarea")
public class TareaController {
	@Autowired 
	TareaRepository tarearepository;
	
	
	//ListarTareas
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<Object> listarTareas(@RequestParam ("size") int page, @RequestParam ("size")int size){
	
		Page<Tarea> tareas=tarearepository.findAll(PageRequest.of(page, size));
		
		return new ResponseEntity<Object>(tareas,HttpStatus.OK);
	}
	
	
	//Guardar Tarea
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Object> AgregarTarea(@RequestBody @Valid Tarea tarea){
			if (tarea==null) 
			return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	return new ResponseEntity<Object>(tarearepository.save(tarea),HttpStatus.OK);

	}
	
	//editar Tarea
	@RequestMapping(method=RequestMethod.PUT, path="{/id}")
	public  ResponseEntity<Object>  editarTarea(@RequestBody @Valid Tarea tarea,
			int id){
		if (tarea==null) 
	return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		
	Optional <Tarea>tareadetalle=tarearepository.findById(id);
	tareadetalle.get().setId_tarea(tarea.getId_tarea());
	tareadetalle.get().setTarea(tarea.getTarea());
	return new ResponseEntity<Object>(tareadetalle.get(),HttpStatus.OK); } 

// Eliminar Tarea

	@RequestMapping(method=RequestMethod.DELETE, path="{/id}")
	public ResponseEntity<Object> eliminarrTarea(@RequestBody @Valid Tarea tarea,
			int id){
		tarearepository.deleteById(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT); }
	} 


