package com.tares.tareas.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tares.tareas.model.Calificacion;
import com.tares.tareas.model.CategoriaTarea;
import com.tares.tareas.repository.CalificacionRepository;
import com.tares.tareas.repository.CategoriaTareaRepository;

public class CalificacionController {

	@RestController
	@RequestMapping(value="V1/calificacion")
	public class CategoriaTareaController {
		
		@Autowired
		CalificacionRepository calificacionRepository;

		//get all categories
		@RequestMapping(method=RequestMethod.GET)
		public ResponseEntity<Object> listarCalificacion(@RequestParam("page") int page, @RequestParam("size") int size){
			
			Page<Calificacion> lista=calificacionRepository.findAll(PageRequest.of(page, size));
			
			return new ResponseEntity<Object>(lista,HttpStatus.OK);
		}
		
		//post category
		
		@RequestMapping(method=RequestMethod.POST)
		public ResponseEntity<Object> agregarCalificacion(@RequestBody @Valid Calificacion calificacion){
			calificacionRepository.save(calificacion);
			
			return new ResponseEntity<Object>(calificacion,HttpStatus.OK);
		}
		
		@RequestMapping(method=RequestMethod.PUT,path="{/id}")
		public  ResponseEntity<Object> EditarCalificacion(@PathVariable(value="id")Integer id,@Valid @RequestBody Calificacion calificacion){
			 		
		 return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		}
		
		@RequestMapping(method=RequestMethod.DELETE,path="{/id}")
		public  ResponseEntity<Object> eliminar(@PathVariable(value="id")Integer id){
		 calificacionRepository.deleteById(id);
		 return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		}
		
}  
	

}
